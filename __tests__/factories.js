const faker = require('faker');
faker.locale = 'pt_BR';
const { factory } = require("factory-girl");
const { Client } = require("../src/app/models");

factory.define("Client", Client, {
  name: faker.name.findName(),
  lastName: faker.name.lastName(),
  birth: faker.date.past(30, 2000),
  cpf: faker.phone.phoneNumber(),
  phone: faker.phone.phoneNumber(),
  email: faker.internet.email(),
  password: faker.internet.password()
});

module.exports = factory;