const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
    const Client = sequelize.define("Client", {
        name: DataTypes.STRING,
        lastName: DataTypes.STRING,
        birth: DataTypes.DATE,
        cpf: DataTypes.STRING,
        phone: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.VIRTUAL,
        passwordHash: DataTypes.STRING,
    }, {
        hooks: {
            beforeSave: async client => {
                if(client.password) {
                    client.passwordHash = await bcrypt.hash(client.password, 8);
                }
            }
        }
    });

    Client.prototype.checkPassword = function(password) {
        return bcrypt.compare(password, this.passwordHash)
    }

    Client.prototype.generateToken = function() {
        return jwt.sign({ id: this.id }, process.env.APP_SECRET);
    }

    return Client;
};